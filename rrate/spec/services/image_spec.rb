# frozen_string_literal: true

require "./lib/services/image"

describe Services::Image do
  let(:img) { Services::Image.new('abc','/source', '/path', 0.5, 1, 2, 3, 4) }

  it "must instantiate with getters" do
    expect(img.id).to eq('abc')
    expect(img.source_path).to eq('/source')
    expect(img.path).to eq('/path')
    expect(img.score).to eq(0.5)
    expect(img.hit).to eq(1)
    expect(img.miss).to eq(2)
    expect(img.hit_count).to eq(3)
    expect(img.miss_count).to eq(4)
  end

  it "must calc score" do
    score = img.calc_score(1.0, 10.0)
    expect(score).to eq(0.1)
  end

  it "must do hit" do
    new_img = img.do_hit(4)
    expect(new_img.score).to eq(0.375)
    expect(new_img.hit).to eq(5)
    expect(new_img.miss).to eq(2)
    expect(new_img.hit_count).to eq(4)
  end

  it "must do miss" do
    new_img = img.do_miss(4)
    expect(new_img.score).to eq(0.125)
    expect(new_img.hit).to eq(1)
    expect(new_img.miss).to eq(6)
    expect(new_img.miss_count).to eq(5)
  end

  it "must to_a" do
    expect(img.to_a).to eq(["/source", "/path", 0.5, 1, 2, 3, 4, "abc"])
  end
end

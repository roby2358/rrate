# frozen_string_literal: true

module Services
  # models a single image in the filesystem
  class Image
    attr_reader :id,
                :source_path,
                :path,
                :score,
                :hit,
                :miss,
                :hit_count,
                :miss_count

    # Initialize the model with the parameters
    def initialize(id, source_path, path, score, hit, miss, hit_count, miss_count)
      @id = id
      @source_path = source_path
      @path = path
      @score = score
      @hit = hit
      @miss = miss
      @hit_count = hit_count
      @miss_count = miss_count
    end

    # Calculate the score based on hits and misses
    def calc_score(hit, miss)
      (1.to_f + hit) / (9.to_f + hit + miss)
    end

    # Create a copy with hit incremented
    def do_hit(add)
      new_hit = @hit + add
      new_hit_count = @hit_count + 1
      Image.new(
        @id,
        @source_path,
        @path,
        calc_score(new_hit, @miss),
        new_hit,
        @miss,
        new_hit_count,
        @miss_count
      )
    end

    # Create a copy with missed incremented
    def do_miss(add)
      new_miss = @miss + add
      new_miss_count = @miss_count + 1
      Image.new(
        @id,
        @source_path,
        @path,
        calc_score(@hit, new_miss),
        @hit,
        new_miss,
        @hit_count,
        new_miss_count
      )
    end

    # do the data
    def do(in_, hit, miss)
      if in_.include?(@id)
        do_hit(hit)
      else
        do_miss(miss)
      end
    end

    # Convert the fields to an array for db operations
    def to_a
      [@source_path, @path, @score, @hit, @miss, @hit_count, @miss_count, @id]
    end
  end
end

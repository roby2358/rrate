# frozen_string_literal: true

# Services for the service
module Services
  # encapsulate db operations
  class ImageDB
    # Initialize
    def initialize(db_path, source_path)
      @db = SQLite3::Database.new(db_path)
      @source_path = source_path
    end

    # Report version of sqlite
    def version
      @db.get_first_value('SELECT SQLITE_VERSION()')
    end

    # Create the tables and indexes
    def create
      sql = [
            "CREATE TABLE IF NOT EXISTS images("\
            ' id STRING PRIMARY KEY,'\
            ' source_path STRING,'\
            ' path STRING,'\
            ' score FLOAT,'\
            ' hit FLOAT,'\
            ' miss FLOAT,'\
            ' hit_count INT,'\
            ' miss_count INT )',
            "CREATE UNIQUE INDEX IF NOT EXISTS images_path"\
            " ON images(path)",
            "CREATE INDEX IF NOT EXISTS images_score"\
            " ON images(score)"
            ]
      p sql
      sql.each { |s| @db.execute(s) }
    end

    # Delete the tables
    def delete
      @db.execute("DROP INDEX IF EXISTS images_path")
      @db.execute("DROP INDEX IF EXISTS images_score")
      @db.execute("DROP TABLE IF EXISTS images")
    end

    # Escape db characters
    def db_escape(str)
      str.gsub("'", "''")
    end

    # Insert an image
    def insert(image)
      @db.execute(
        "INSERT INTO images VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        [
          image.id,
          db_escape(@source_path),
          db_escape(image.path),
          image.score,
          image.hit,
          image.miss,
          image.hit_count,
          image.miss_count
        ]
      )
    rescue StandardError => e
      p("Error inserting path='#{image.path}' error=#{e}")
    end

    # Count images for the source_path
    def count
      statement = "SELECT COUNT(*) FROM images"\
                  " WHERE source_path='#{@source_path}'"
      @db.get_first_value(statement)
    end

    # Fetch the image at offset
    def fetch(offset)
      statement = "SELECT * FROM images"\
                  " WHERE source_path='#{@source_path}'"\
                  ' LIMIT 1 OFFSET ?'
      row = @db.get_first_row(statement, offset)
      Image.new(*row)
    end

    # Reload random images
    def reload_random(num)
      n = count

      sample = Set[]
      while sample.size < num do sample.add(rand(n)) end

      sample.to_a.shuffle.map { |i| attributes(DB.fetch(i)) }
    end

    # Reload the top images
    def reload_top(num)
      statement = "SELECT * FROM images"\
                  " WHERE source_path='#{@source_path}'"\
                  ' ORDER BY SCORE DESC'\
                  ' LIMIT ?'
      rows = @db.execute(statement, num)
      rows.map { |r| attributes(Image.new(*r)) }
    end

    # Reload the bottom images
    def reload_bottom(num)
      statement = "SELECT * FROM images"\
                  " WHERE source_path='#{@source_path}'"\
                  ' ORDER BY SCORE LIMIT ?'
      rows = @db.execute(statement, num)
      rows.map { |r| attributes(Image.new(*r)) }
    end

    # Bump an image up
    def up(image)
      statement = "UPDATE images SET source_path=?, path=?, score=?,"\
                  ' hit=?, miss=?, hit_count=?, miss_count=? WHERE id=?'
      p statement
      @db.execute(statement, image.to_a)
    end

    def placeholders(length)
      (['?'] * length).join(',')
    end

    # Submit data to the db
    def submit(in_, out, hit, miss)
      # NOTE this is really messed up
      ids = (out.to_set + in_.to_set).to_a
      statement = "SELECT * FROM images"\
                  " WHERE id in (#{placeholders(ids.length)})"
      images = @db.execute(statement, ids)
      images.each do |ii|
        image = Image.new(*ii)
        # NOTE this is really messed up
        new_image = image.do(in_, hit, miss)
        p new_image
        up(new_image)
      end
    end

    # Close the db
    def close
      @db&.close
    end
  end
end

# frozen_string_literal: true

# Services for the servlet
module Services
  IMAGE_EXT = Set['bmp', 'jpg', 'jpeg', 'png', 'gif']
  public_constant :IMAGE_EXT

  # Methods for finding images in a directory
  class ImageDir
    # Initialize
    def initialize(source_path)
      @source_path = source_path
    end

    # Returns the extention of the file, or nil
    def ext(path)
      i = path.rindex('.')
      i.nil? ? nil : path[(i + 1)..-1].downcase
    end

    # Return the path if it's for an image, or nil
    def imagefile?(path)
      e = ext(path)
      e.nil? || !IMAGE_EXT.include?(e) ? nil : path
    end

    # Look at a path to see if we need to traverse or apply the function
    def apply(path, &func)
      if File.directory?(path)
        p("dir: #{path}/")
        walk(path, &func)
      else
        func.call(path.delete_prefix(@source_path))
      end
    end

    # traverse a path and apply the function
    def walk(path, &func)
      Dir.foreach(path) do |x|
        apply(File.join(path, x), &func) unless ['.', '..'].include?(x)
      end
    end

    # Traverse to find all extentions
    def extentions
      all = Set[]
      walk(@source_path) do |path|
        e = ext(path)
        e && all.add(e)
      end
      all
    end

    # Traverse the directory and apply the function to each image file
    def image_files
      walk(@source_path) do |path|
        yield path if imagefile?(path)
      end
    end
  end
end

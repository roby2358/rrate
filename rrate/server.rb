#! /usr/bin/ruby

# frozen_string_literal: true

# Requires the Gemfile

require 'bundler'
Bundler.require
require 'sqlite3'


# Pretty print
require 'pp'

require './lib/services/image_dir.rb'
require './lib/services/image_db.rb'
require './lib/services/image.rb'

# see https://github.com/rmagick/rmagick
# see https://www.amberbit.com/blog/2013/12/20/similar-images-detection-in-ruby-with-phash/

use Rack::Static, :urls => { '/' => 'index.html' }, :root => 'public', :index => 'index.html'

set :port, 8888
set :bind, '0.0.0.0'

DATABASE_PATH = 'rrate.db'
PUBLIC_PATH = '/home/vagrant/rrate/public'
BASE_URL = '/source/faces/done'
SOURCE_PATH = PUBLIC_PATH + BASE_URL

DB = Services::ImageDB.new(DATABASE_PATH, SOURCE_PATH)
DIR = Services::ImageDir.new(SOURCE_PATH)

before do
  # parse body as json
  request.body.rewind
  b = request.body.read
  @request_payload = b.nil? || b.empty? ? nil : (JSON.parse b)

  # write json
  content_type :json
end

get '/hello-world' do
  logger.info 'Request headers:'
  logger.info request.env.class
  logger.info request.env
  logger.info 'User agent: ' + request.env['HTTP_USER_AGENT']
  logger.info params[:i]

  { 'message' => 'Hello World!' }.to_json
end

post '/post' do
  logger.info @request_payload
  logger.info @request_payload['i']
  { 'message' => 'OK' }.to_json
end

get '/extentions' do
  logger.info 'Get extention list'
  all = DIR.extentions
  { 'image' => (all.intersection Services::IMAGE_EXT).to_a.sort,
    'unused' => (Services::IMAGE_EXT.difference all).to_a.sort,
    'other' => (all.difference Services::IMAGE_EXT).to_a.sort }.to_json
end

get '/files' do
  logger.info 'Get file list'

  DB.create

  count = 0
  DIR.image_files do |path|
    count += 1
    DB.insert(Services::Image.new(
      SecureRandom.uuid,
      SOURCE_PATH,
      path,
      0.1,
      0.0,
      0.0,
      0,
      0))
  end
  { 'scanned' => count, 'saved' => DB.count }.to_json
end

def attributes(obj)
  obj.instance_variables
   .each_with_object({}) do |var, hash|
      hash[var.to_s.delete('@')] = obj.instance_variable_get(var)
   end
end

def response_sample(paths)
  { 'sample': paths, 'base_url': BASE_URL }.to_json
end

get '/sample' do
  num = (params[:num] || '9').to_i
  paths = DB.reload_random(num)
  response_sample(paths)
end

get '/top' do
  num = (params[:num] || '9').to_i
  paths = DB.reload_top(num)
  response_sample(paths)
end

get '/bottom' do
  num = (params[:num] || '9').to_i
  paths = DB.reload_bottom(num)
  response_sample(paths)
end

post '/submit' do
  p @request_payload
  in_ = @request_payload.dig('in') || []
  out = @request_payload.dig('out') || []
  hit = @request_payload.dig('hit') || 0
  miss = @request_payload.dig('miss') || 0
  DB.submit(in_, out, hit, miss)
  { 'message' => 'OK' }.to_json
end

get '/sqlite' do
  { 'sqliteVersion': DB.version }.to_json
end

get '/reset' do
  DB.delete
  { 'message' => 'OK' }.to_json
end

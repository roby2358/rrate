# Ruby Tools

- apt install ruby-rspec-core
- apt install dos2unix -y
- apt install rerun -y
- gem install rouge -y
- apt install gcc -y
- apt install make -y
- apt install pry

# Starting the Server

- vagrant ssh
- cd rrate
- ruby server.rb

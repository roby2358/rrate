/*
Copyright (c) 2014 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 22, 2014

@author: roby
*/

$.Images = function() {
  var THIS = this;

  function update(data) {
    console.log(data);
    console.log(data.base_url);
    THIS.images = data.sample;
    for (var i = 0 ; i < THIS.images.length ; i++) {
      var s = THIS.images[i]
      var alt = s.path;
      var url = encodeURI(data.base_url + alt);
      console.log("url", s.path);
      console.log("score", s.score);
      var p = $("#preview" + i);
      p.attr("src", url);
      p.attr("alt", alt);
      p.attr("imageid", s.id);
      p.attr("score", s.score);
    }
  }

  function load() {
    $.get("/sample", update);
  }

  function loadTop() {
    console.log("loadTop");
    $.get("/top", update);
  }

  function loadBottom() {
    console.log("loadBottom");
    $.get("/bottom", update);
  }

  function hit(i) {
    // hit adds the sum of other image scores
    var sum = 0.0;
    for (var j = 0 ; j < THIS.images.length ; j++)
      if (i != j)
        sum += THIS.images[i].score;
    return sum;
  }

  function miss(i) {
    // miss is the selected score, inverted in an s-curve
    var score = THIS.images[i].score;
    return (1.0 - score * score) / (1.0 + score * score);
  }

  function roll_up(i) {
    var iin = [];
    var out = [];
    for (var ii = 0 ; ii < THIS.images.length ; ii++ ) {
      if (ii == i)
        iin.push(THIS.images[i].id);
      else
        out.push(THIS.images[ii].id);
    }
    return {
      "in": iin,
      "out": out,
      "hit": hit(i),
      "miss": miss(i)
    }
  }

  function select(i) {
    if (! THIS.images) return;

    console.log("select", i, hit(i), miss(i));
    console.log(THIS.images[i].path);

    var data = roll_up(i);
    console.log(data);
    $.post("/submit", JSON.stringify(data));
  }

  function open(i) {
    console.log(prefix, THIS.images[i].path);
    window.open(prefix + THIS.images[i].path, "_blank");
  }

  THIS.load = load;
  THIS.loadTop = loadTop;
  THIS.loadBottom = loadBottom;
  THIS.select = select;
  THIS.open = open;
}